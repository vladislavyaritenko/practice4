package ua.nure.yaritenko.Practice4.part4;


import ua.nure.yaritenko.Practice4.Util;

import java.io.IOException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part4 implements Iterable<String> {
    static String input;
    final static String REG = "(?Umi)((\\-\\s+)?\\w+(\\-\\w+)?[,:;]?\\s+){2,}(\\-\\s+)?\\w+(\\-\\w+)?[.!?]+\\s*";

    private class IteratorImpl implements Iterator<String> {
        Pattern pattern;
        Matcher matcher;

        IteratorImpl() {
            pattern = Pattern.compile(REG);
            matcher = pattern.matcher(input);
        }

        public boolean hasNext() {
            return matcher.find();
        }

        public String next() {
            return matcher.group();
        }

        public void remove() {
            throw new UnsupportedOperationException("Called method remove");
        }
    }

    public Iterator<String> iterator() {
        return new IteratorImpl();
    }

    public static String convert1() {
        StringBuilder sb = new StringBuilder();
        for (Iterator<String> iterator = new Part4().iterator(); iterator.hasNext(); ) {
            sb.append(iterator.next());
        }
        return sb.toString();
    }

    public static void main(String[] args) throws IOException {
        input = Util.getInput("part4.txt");
        System.out.println("===Part 4");
        System.out.println(Part4.convert1());
    }
}