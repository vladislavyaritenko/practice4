package ua.nure.yaritenko.Practice4.part1;

import ua.nure.yaritenko.Practice4.Util;

import java.io.FileNotFoundException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part1 {
    public static String convert1(String input) {
        final StringBuffer sb = new StringBuffer();
        final Matcher matcher = Pattern.compile("(?Um)\\w{4,}").matcher(input);
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group().toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(Part1.convert1(Util.getInput("part1.txt")));
    }
}
