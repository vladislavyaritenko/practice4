package ua.nure.yaritenko.Practice4.part2;

import ua.nure.yaritenko.Practice4.Util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part2 {
    private static void rand() throws IOException {
        Random random = new Random();
        int[] mas = new int[10];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = random.nextInt(50);
        }
        putInput2(mas, "part21.txt");
    }

    private static void putInput2(int[] array, String nameFile) throws IOException {
        try (final FileWriter writer1 = new FileWriter(nameFile, false)) {
            for (int anArray : array) {
                final String s = Integer.toString(anArray);
                writer1.write(s);
                writer1.write(" ");
            }
        }
    }

    public static void sort(int[] arr) {
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int t = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = t;
                }
            }
        }
    }

    public static void convert1(String input) throws IOException {
        Pattern pattern = Pattern.compile("(?Umi)(\\d+)(\\W*)");
        Matcher matcher = pattern.matcher(input);
        int[] mas = new int[10];
        int count = 0;
        while (matcher.find()) {
            mas[count] = Integer.parseInt(matcher.group(1));
            count++;
        }
        sort(mas);
        putInput2(mas, "part22.txt");
    }

    public static void main(String[] args) throws IOException {
        Part2.rand();
         StringBuilder sb = new StringBuilder();
        String input = Util.getInput("part21.txt");
        sb.append("input ==> ").append(input).append(System.lineSeparator());
        Part2.convert1(input);
        File file = new File("part22.txt");
        if (file.exists()) {
            sb.append("output ==> ").append(Util.getInput("part22.txt"));
        }
        System.out.println(sb);
    }

}
