package ua.nure.yaritenko.Practice4.part5;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part5 {

    private static String convert1(String key, String local) {
        StringBuilder sb = new StringBuilder();

        ResourceBundle rb = ResourceBundle.getBundle("resources_" + local);
        sb.append(rb.getString(key.toLowerCase()));
        return sb.toString();

    }

    public static void main(String[] args) throws IOException {
        System.out.println("===Part 5");
        Scanner sc = new Scanner(System.in);
        Pattern pattern = Pattern.compile("(?Umi)(\\w+)");
        Matcher matcher;
        String input;
        String[] out = {"", "", ""};
        int count;
        while (!((out[2]).equals("stop"))) {
            System.out.println("Введите ключ (key) и имя локализации (RU or EN) через пробел! ");
            input = sc.nextLine();
            matcher = pattern.matcher(input);
            count = 0;
            while(matcher.find()){
                out[count] = matcher.group();
                count++;
            }
            System.out.println(Part5.convert1(out[0], out[1]));
            System.out.println("Введите любой символ для продолжения, 'stop' - для завершения");
           out[2] = sc.nextLine();
        }
    }
}
