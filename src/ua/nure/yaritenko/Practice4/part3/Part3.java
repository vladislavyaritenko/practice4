package ua.nure.yaritenko.Practice4.part3;

import ua.nure.yaritenko.Practice4.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part3 {
    public static String convert1(String input, String type) {
        StringBuilder strOut = new StringBuilder();
        String reg;
        Pattern pattern;
        Matcher matcher;
        switch (type) {
            case "char": {
                reg = "(?Um)(\\S+)";
                pattern = Pattern.compile(reg);
                matcher = pattern.matcher(input);
                while (matcher.find()) {
                    if (matcher.group().length() == 1)
                        strOut.append(matcher.group()).append(" ");
                }
                strOut.deleteCharAt(strOut.length() - 1);
                break;
            }
            case "int": {
                reg = "(?m)((\\.?)\\d+(\\.\\d+)?)";
                pattern = Pattern.compile(reg);
                matcher = pattern.matcher(input);
                while (matcher.find()) {
                    double d = Double.parseDouble(matcher.group());
                    int i = (int) d;
                    String str = String.valueOf(i);
                    String str2 = matcher.group();
                    if (str.equals(str2)) {
                        strOut.append(matcher.group()).append(" ");
                    }
                }
                strOut.deleteCharAt(strOut.length() - 1);
                break;
            }
            case "String": {
                reg = "(?Umi)([\\w&&[^\\d]]{2,})";
                pattern = Pattern.compile(reg);
                matcher = pattern.matcher(input);
                while (matcher.find()) {
                    strOut.append(matcher.group()).append(" ");
                }
                strOut.deleteCharAt(strOut.length() - 1);
                break;
            }
            case "double": {
                reg = "(?Umi)(\\d*\\.\\d+)+";
                pattern = Pattern.compile(reg);
                matcher = pattern.matcher(input);
                while (matcher.find()) {
                    strOut.append(matcher.group()).append(" ");
                }
                strOut.deleteCharAt(strOut.length() - 1);
                break;
            }
        }


        return strOut.toString();
    }

    public static void main(String[] args) throws IOException {
        String input = Util.getInput("part3.txt");
        System.out.println(input);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String s;
        System.out.println("Введите один из типов данных: char, int, String, double! Или \"стоп\" для остановки");
        while (in.ready()) {
            s = in.readLine();
            if(s.equals("stop")){
                break;
            }
            System.out.println(Part3.convert1(input, s));

        }
    }

}
