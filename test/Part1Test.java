import org.junit.Test;
import ua.nure.yaritenko.Practice4.Util;
import ua.nure.yaritenko.Practice4.ua.nure.yaritenko.Practice4.part1.Part1;

import static org.junit.Assert.*;

public class Part1Test {
    @Test
    public void main()  {
         Part1 part1 = new Part1();
         Util util = new Util();
        String input = Util.getInput("part1.txt");
        String input1 = Util.getInput("par2t1.txt");
        String expected = "WHEN I was YOUNGER so MUCH YOUNGER THAN TODAY" + System.lineSeparator() +
                "ДЕЛАЕТ КОНЕЧНЫМ СОСТОЯНИЕ ВЫВОДА ОЧИЩАЯ все БУФЕРА в том, ЧИСЛЕ и БУФЕРА ВЫВОДА" + System.lineSeparator() +
                "РОБИТЬ КІНЦЕВИМ СТАН ВИВЕДЕННЯ ОЧИЩАЮЧИ все БУФЕРА в ТОМУ ЧИСЛІ і БУФЕРА ВИВОДУ";
        String actual = Part1.convert1(input);
        assertEquals(expected, actual);
        Part1.main(new String[]{});
    }


}