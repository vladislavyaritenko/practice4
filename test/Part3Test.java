import org.junit.Test;
import ua.nure.yaritenko.Practice4.ua.nure.yaritenko.Practice4.part3.Part3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * Created by Oleg on 04.12.2017.
 */
public class Part3Test {
    @Test
    public void Char() throws Exception {
        String input = "a bcd 43.43 432 и л фвыа 89 .98";
        String expected = "a и л";
        String actual = Part3.convert1(input, "char");
        assertEquals(expected, actual);
    }
    @Test
    public void Int() throws Exception {
        String input = "a bcd 43.43 432 и л фвыа 89 .98";
        String expected = "432 89";
        String actual = Part3.convert1(input, "int");
        assertEquals(expected, actual);
    }
    @Test
    public void Str() throws Exception {
        String input = "a bcd 43.43 432 и л фвыа 89 .98";
        String expected = "bcd фвыа";
        String actual = Part3.convert1(input, "String");
        assertEquals(expected, actual);
    }
    @Test
    public void Doub() throws Exception {
        String input = "a bcd 43.43 432 и л фвыа 89 .98";
        String expected = "43.43 .98";
        String actual = Part3.convert1(input, "double");
        assertEquals(expected, actual);
    }
    @Test
    public void main() throws IOException {
        Part3 part3 = new Part3();
        System.setIn(new
                ByteArrayInputStream("char\nString\nint\ndouble\nstop".getBytes("UTF-8")));
        Part3.main(new String[]{});
    }

}