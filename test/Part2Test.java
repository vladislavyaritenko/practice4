import org.junit.Test;
import ua.nure.yaritenko.Practice4.Util;
import ua.nure.yaritenko.Practice4.ua.nure.yaritenko.Practice4.part2.Part2;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.*;

public class Part2Test {

    @Test
    public void convert1() throws Exception {
        Part2 part2 = new Part2();
        Util util = new Util();
        String input = Util.getInput("part21.txt");
        String strArr[] = input.split(" ");
        int numArr[] = new int[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            numArr[i] = Integer.parseInt(strArr[i]);
        }
        Arrays.sort(numArr);
        Part2.convert1(input);
        String actual = Util.getInput("part22.txt");
        String strArr1[] = actual.split(" ");
        int numArr1[] = new int[strArr1.length];
        for (int i = 0; i < strArr1.length; i++) {
            numArr1[i] = Integer.parseInt(strArr1[i]);
        }
        assertArrayEquals(numArr, numArr1);
        Part2.main(new String[]{});
    }

    @Test
    public void sort() {
        String expected = "1 9 10 19 27 35 35 40 43 48";
        String strArr[] = expected.split(" ");
        int numArr[] = new int[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            numArr[i] = Integer.parseInt(strArr[i]);
        }
        String actual = Util.getInput("part2SortTest.txt");
        String strArr1[] = actual.split(" ");
        int numArr1[] = new int[strArr1.length];
        for (int i = 0; i < strArr1.length; i++) {
            numArr1[i] = Integer.parseInt(strArr1[i]);
        }
        Part2.sort(numArr1);
        assertArrayEquals(numArr, numArr1);
    }




}